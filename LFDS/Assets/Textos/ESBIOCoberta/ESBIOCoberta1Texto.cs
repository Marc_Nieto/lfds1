﻿	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class ESBIOCoberta1Texto : MonoBehaviour {
	public static ESBIOCoberta1Texto Instance { get; private set;}
	public static string[] menu_laterial1 = { "Ciutat\nSostenible", "Castellano", "English" };

	public static string[] menu_laterial2 = { "Estil de vida\nSostenible", "Castellano", "English" };

	public static string[] menu_laterial3 = { "Edifici\nSostenible", "Castellano", "English" };

		public static string[] seccion = { "La coberta verda de la Fábrica del Sol", "Castellano", "English" };

		public static string[] titulo = { "Què és i com funciona la coberta verda de\nLa Fábrica del Sol?", "Castellano", "English" };

	public static string[] contenido = { "La coberta verda és l’espai del terrat enjardinat i on es poden\nrefugiar diversos animals. És una manera de transformar terrats,\nespais en desús o encimentats en un jardí o un hort.\n\nEs considera coberta enjardinada aquella que té una àrea de\nvegetació, continguda en jardineres, major al 30% de la superfície\ndel terrat. ", "Castellano", "English" };

		public static string[] titulo_imagen = { "Quines són les principals funcions d’una coberta verda?", "Castellano", "English" };

		public static string[] rombo_jardin = { "Insectes", "Castellano", "English" };

	public static string[] texto_jardin = { "La coberta verda és un reservori per\na les espècies animals de la ciutat: \nun refugi i aliment per a insectes, aus i altres \nanimals de la fauna urbana, alhora que \nsón espais de relax per a nosaltres.", "Castellano", "English" };

		public static string[] rombo_placa = { "Aïllant", "Castellano", "English" };

	public static string[] texto_placa = { "La coberta verda actua com a aïllant\ntèrmic: redueix el consum d'energia,\nsmantenint fresc l'edifici a l'estiu a través de\nprocés de transpiració de les plantes, el \nque redueixla necessitat d'aire condicionat.", "Castellano", "English" };

		public static string[] rombo_cielo = { "Oxígen", "Castellano", "English" };

	public static string[] texto_cielo = { "La coberta verda oxigena la ciutat:\n les plantes en zones urbanes aporten \noxigen i “netegen“ l’aire.", "Castellano", "English" };

		public static string[] rombo_techo = { "Temperatura", "Castellano", "English" };

	public static string[] texto_techo = { "La coberta verda mitiga l’efecte \nilla de calor, reduint la temperatura.", "Castellano", "English" };

	public static string[] bocadillo1 = { "Ves al terrat per descobrir\n la coberta verda! :)", "Castellano", "English" };

		public static string[] bocadillo2 = { "No t’oblidis d’anar  al terrat per descobrir la coberta verda", "Castellano", "English" };
    public static string[] desplegable = { "La coberta verda de La Fàbrica del Sol\n\nLa vegetació a La Fàbrica del Sol, naturalitzada\n\nLa Fábrica del Sol, refugi per a la fauna urbana\n\nTest", "Castellano", "English" };

    }
	
