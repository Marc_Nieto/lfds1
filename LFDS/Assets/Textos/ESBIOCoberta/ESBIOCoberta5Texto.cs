﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESBIOCoberta5Texto : MonoBehaviour {
    public static string[] bocadillo={"Descobreix les especies\nplantades a la coberta de\nLa Fàbrica del Sol :)", "Castellano", "English" };
	public static string[] seccion = { "La vegetació a La Fàbrica del Sol, naturalizada", "Castellano", "English" };
    public static string[] titulo = { "Com és la vegetació de La Fábrica del Sol?", "Castellano", "English" };
    public static string[] contenido = { "La vegetació de la coberta de LFdS està formada per una combinació\nde 15 espècies de plantes arbustives i vivaces.\n\nEn la rehabilitació de l'edifici s'han augmentat la biodiversitat i el\nnombre de plantes autòctones, per aconseguir uns rangs de floració\nmés distribuïts al llarg de l'any, i s'han inclòs més plantes amb\nfloració atraient de fauna. Cada planta té les seves característiques,\nel seu color i l seva època de floració i diferents necessitats de\nmanteniment i d'aigua.", "Castellano", "English" };
    public static string[] desplegable = { "La coberta verda de La Fàbrica del Sol\n\nLa vegetació a La Fàbrica del Sol, naturalitzada\n\nLa Fábrica del Sol, refugi per a la fauna urbana\n\nTest", "Castellano", "English" };
}
