﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESBIOCoberta6Texto : MonoBehaviour {

    public static string[] bocadillo = { "Navega al mapa!", "Castellano", "English" };
    public static string[] seccion = { "La vegetació a La Fàbrica del Sol, naturalizada", "Castellano", "English" };
    public static string[] titulo= { "Quins espais verds naturals i d'interés estacionals\nhi ha a Barcelona?", "Castellano", "English" };
    public static string[] contenidoFlor = { "Espai d'interès estacional: Espai on són patents els canvis estacionals o fenològics en\nels organismes presents com floraciones, pèrdua o emergència de fulles, arribada, pas o partida\nd'ocells migradors, cant de granotes, etc.", "Castellano", "English" };
    public static string[] contenidoArbol = { "Espai d'inerès natural: És un espai amb valors naturals rellevants, que\ncontenen exemples de la flora, la fauna i el paisatge locals, i sovint també tenen interès a l'hora\nd'explicar la història de la ciutat: Inclou espaís formalment protegits, com ara Collserola.", "Castellano", "English" };
    public static string[] contenidoJardin = { "Parc o jardí forestal o assilvestrat: Parc o jardí de caràcter assilvestrat, amb masses\narbustives, praderes de gramínides, predomini de bosc i zones habilitades com a refugi de fauna.", "Castellano", "English" };
    public static string[] contenidoCactus = { "Col·lecció botànica: Espai de col·lecció dedicat a unes espècies vegetals determinades,\ncosa que els otroga una important funció divulgativa i formativa en el camp de la botànica. Per \nexemple, jardins de cactus, plantes bulboses, roserars, etcètera.", "Castellano", "English" };
    public static string[] desplegable = { "La coberta verda de La Fàbrica del Sol\n\nLa vegetació a La Fàbrica del Sol, naturalitzada\n\nLa Fábrica del Sol, refugi per a la fauna urbana\n\nTest", "Castellano", "English" };
}
