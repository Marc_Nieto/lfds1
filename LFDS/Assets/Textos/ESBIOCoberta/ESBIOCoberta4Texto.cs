﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESBIOCoberta4Texto : MonoBehaviour {
public static string[] bocadillo = { "Navega al mapa!", "Castellano", "English" };
public static string[] titulo = { "Vols saber-ne més?", "Castellano", "English" };
public static string[] seccion = { "La coberta verda de la Fábrica del Sol", "Castellano", "English" };
public static string[] mosaic_1 = { "Cobertes mosaic", "Castellano", "English" };
public static string[] mosaic_3 = { "Ciutadania\ncorresponsable", "Castellano", "English" };
public static string[] mosaic_2 = { "Resiliència i\ncanvi climàtic", "Castellano", "English" };
public static string[] desplegable = { "La coberta verda de La Fàbrica del Sol\n\nLa vegetació a La Fàbrica del Sol, naturalitzada\n\nLa Fábrica del Sol, refugi per a la fauna urbana\n\nTest", "Castellano", "English" };
public static string[] contenido = { "", "", "" };
}
