﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESBIOCoberta10Texto : MonoBehaviour {
    public static string[] bocadillo = { "Navega al mapa!", "Castellano", "English" };
    public static string[] seccion = { "La Fàbrica del Sol, refugi per la fauna urbana", "Castellano", "English" };
    public static string[] titulo = { "Quins altres indrets a Barcelona donen\nrefugi a la fauna urbana?", "Castellano", "English" };
    public static string[] lagarto = { "Fauna: Indret especificament interessant per la fauna\npresent", "Castellano", "English" };
    public static string[] Agua = { "Ambient aquàtic continental: Autèntic\nmicrosistema aquàtic, que tot i ser generalment artificial,\nsovint ofereix una gran biodiversitat. Inclou basses,\npantans, espais fluvials i zones humides en general", "Castellano", "English" };
    public static string[] Grulla = { "Hàbitat litoral: Zona litoral on es concentra flora i\nfauna silvestre, ja sigui sota l'aigua, a la superficie, a la\ncosta o en terrenys adjacents. Cal utilitzar aquesta icona\nde manera sensible amb els hàbitats fràgils. No s'han\n d'assenyalar les zones de nidificació sense indicar que\ncal tenir-hi precaució.", "Castellano", "English" };
}
