﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESBIOCoberta7Texto : MonoBehaviour {
public static string[] seccion = { "La vegetació a La Fàbrica del Sol, naturalizada", "Castellano", "English" };
public static string[] titulo = { "Vols conèixer les millors\npràctiques?", "Castellano", "English" };
    public static string[] rectangulo1 = { "Jardí d'especial interès per\na la biodiversitat del Parc de\nJoan Miró", "Castellano", "English" };
    public static string[] rectangulo1Dentro = { "Jardí d'especial interès per a la\nbiodiversitat del Parc de Joan Miró", "Castellano", "English" };
    public static string[] rectangulo2 = { "La biodiversitat a Montjuïc", "Castellano", "English" };
    public static string[] descripcion_rectangulo1 = { "Es un ambient amb la selecció de les plantes i\nla creació de refugis que atrauen ocells,\npapallones i altres animals. La parcel·la, de\n450 m2, és un indret lliure d'espècies\ninvasores, conegut com el Jardí de les\npapallones. Tan sols hi poden accedir jardiners\ni aquelles aus, insectes o animals invertebrats\nconsiderats beneficiosos i als quals s'intenta\natreure perque se sentin aquí com a casa.", "Castellano", "English" };
    public static string[] descripcion_rectangulo2 = { "En l'estructura verda de la ciutat, la muntanya\nde Montjuïc destaca per una major biodiversitat\nbotànica i faunística, exceptuant Collserola.\nEs tracta d'un espao que com a ecosistema\nnatural dins de la ciutat és singular,\nja que està integrat per un mosaic d'ambients\nque reuneix en la seva superfície pràcticament\ntots el biòtops dels contorns de Barcelona,\nalguns dels quals, com el penya-segat del front marítim,\nd'un gran valor ecològic.", "Castellano", "English" };
    public static string[] contenido = { "", "", "" };
    public static string[] desplegable = { "La coberta verda de La Fàbrica del Sol\n\nLa vegetació a La Fàbrica del Sol, naturalitzada\n\nLa Fábrica del Sol, refugi per a la fauna urbana\n\nTest", "Castellano", "English" };

}
