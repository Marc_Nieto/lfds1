﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESBIOCoberta3Texto : MonoBehaviour {
public static string[] titulo = { "Vols conèixer les millors\npràctiques?", "Castellano", "English" };
public static string[] seccion = { "La coberta verda de la Fábrica del Sol", "Castellano", "English" };
public static string[] rectangulo1 = { "Casa Cambó", "Castellano", "English" };
public static string[] descripcion_rectangulo1 = {"La primera coberta verda que es va instal·lar a Barcelona\nés el jardí en alçada de la Casa Cambó, a Via Laietana 30.\nVa ser dissenyat per Nicolau Maria Rubió i Tudurí,\narquitecte i paisatgista, i director de\nParcs i Jardins de l’Ajuntament de Barcelona des de 1917.\nDesprés de la Guerra Civil el jardí va ser reconstruït per\nJoan Mirambell i Ferran. Aquesta coberta verda de més\nde 1.000 m2 segueix sent una de les més significatives de\nBarcelona.", "Castellano", "English" };
public static string[] rectangulo2 = { "Biblioteca Joan Maragall\nVil·la Florida", "Castellano", "English" };
public static string[] descripcion_rectangulo2 = { "Per mantenir al màxim l’espai dels jardins de la Vil·la Florida,\nla Biblioteca Joan Maragalls’ha construït com a un edifici\nsemisoterrat amb un jardí a la coberta.\nÉs una intervencióque millora les condicions\ndel carrer i facilita la connexió amb el jardí de Vil·la Florida.\nL’espai interior es fragmenta creant àmbits d’escala\nhumana d’una mida confortable i moltben il·luminats,\nmitjançant patis útils que permeten la privacitat en un\nlloc públic. Peraquesta solució els autors, l’estudi BCQ,\nvan rebre el Premi Ciutat de Barcelonad’Arquitectura\ni Urbanisme l’any 2014.", "Castellano", "English" };
public static string[] contenido = { "", "", "" };
public static string[] desplegable = { "La coberta verda de La Fàbrica del Sol\n\nLa vegetació a La Fàbrica del Sol, naturalitzada\n\nLa Fábrica del Sol, refugi per a la fauna urbana\n\nTest", "Castellano", "English" };
}
