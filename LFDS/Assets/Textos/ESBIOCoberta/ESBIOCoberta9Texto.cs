﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESBIOCoberta9Texto : MonoBehaviour {
    public static string[] seccion = { "La Fàbrica del Sol, refugi per la fauna urbana", "Castellano", "English" };
    public static string[] titulo = { "Com dona refugi a la fauna urbana\nLa Fàbrica del Sol?", "Castellano", "English" };
    public static string[] contenido_5 = { "A La Fàbrica del Sol s'han creat estructures per donar espais de\nrefugi, d'alimentació i per a la reproducció de la fauna urbana.", "Castellano", "English" };
    public static string[] cuadrado_1 = { "Edifici amigable\namb els ocells", "Castellano", "English" };
    public static string[] cuadrado_2 = { "Hotel d'insectes", "Castellano", "English" };
    public static string[] cuadrado_3 = { "Caixas niu", "Castellano", "English" };
    public static string[] bocadillo4 = { "Descobreix els espais per la\nfauna a la coberta, fixa't en\nla façana i descobriràs els\nnius d'ocells!:)", "Castellano", "English" };
    public static string[] descripcion_cuadrado1 = { "La Fàbrica del Sol és un edifici amigable\namb els ocells: s'han adaptat les obertures\nde ventilació existents per tal que espècies\nautòctones d'ocells com la gralla hi\nnidifiquin. Així recuperem el patrimoni\nnatural de la ciutat tot potenciat la\npreséncia d'ocells d'interès per a la millora\nde la biodiversitat i reduint la dels coloms.", "Castellano", "English" };
    public static string[] descripcion_cuadrado2 = { "La instal·lació d'un hotel d'insectes a\nla coberta afavoreix la presència\nd'insectes pol·linitzadors, com les\nabelles o vespes solitàries.", "Castellano", "English" };
    public static string[] descripcion_cuadrado3 = { "Tambè hi ha caixes niu per a ratpenats, que\ntenen dificultats de trobar refugis adequats en\nforma de forats o escletxes en la ciutat.", "Castellano", "English" };
}
