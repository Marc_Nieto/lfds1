﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESBIOCoberta11Texto : MonoBehaviour {
    public static string[] seccion = { "La Fàbrica del Sol, refugi per la fauna urbana", "Castellano", "English" };
    public static string[] titulo = { "Vols coneixer les millors\npràctiques?", "Castellano", "English" };
    public static string[] rectangulo1 = { "Hotel d'insectes del Jardí Botànic", "Castellano", "English" };
    public static string[] rectangulo2 = { "Naturalització de basses", "Castellano", "English" };
    public static string[] descripcion_rectangulo1 = { "El hotel d'insectes del Jardí Botànic de Barcelona utilitza materials variats\ncom troncs de fusta, canyes, totxos, argila i materials vegetals per crear\nrefugi per abelles, vespes i altres insectes pol·linitzadors. La varietat\nd'insectes ajuda a que una major part de kes 1.700 espècies de plantes del\nJardí Botànic tinguin pol·linitzadors adients.", "Castellano", "English" };
    public static string[] descripcion_rectangulo2 = { "Des del 2008 es du a terme aquest projecte de recuperació i creació d'hàbitats\naquàtics d'aigua dolça. Es tracta de aconseguir l'equilibri natural de la\nflora i fauna de la bassa, i de crear estructures com rampes que connectin\nl'interior amb l'exterior de la bassa. S'han naturalitzat més de 100 estanys\npúblics i privats.", "Castellano", "English" };
}
