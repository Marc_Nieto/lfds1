﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESBIOCoberta2Texto : MonoBehaviour {
public static string[] bocadillo = { "Navega al mapa!", "Castellano", "English" };
public static string[] seccion = { "La coberta verda de la Fábrica del Sol", "Castellano", "English" };
public static string[] titulo = { "Quines altres cobertes verdes hi ha a Barcelona?", "Castellano", "English" };
public static string[] contenido = { "La coberta o sostre amb vegetació que aporta beneficis\nambientals i serveix d'hàbitat per a la fauna i la flora. Les\ncobertes verdes intensives, de manteniment intensiu, es\npoden utilitzar com a jardí i les cobertes verdes extensives,\namb requeriments de manteniment baixos, es dissenyen\namb una finalitat estètica i ambiental. També reben el\nnom de cobertes ecològiques, teulades amb vegetació,\ncobertes-jardí...", "Castellano", "English" };
public static string[] desplegable = { "La coberta verda de La Fàbrica del Sol\n\nLa vegetació a La Fàbrica del Sol, naturalitzada\n\nLa Fábrica del Sol, refugi per a la fauna urbana\n\nTest", "Castellano", "English" };
}
