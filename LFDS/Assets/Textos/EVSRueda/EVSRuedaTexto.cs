﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EVSRuedaTexto : MonoBehaviour {

    public static string[] bocadillo = { "Vols saber com tu\nmateix pots ser \nmés sostenible?", "Castellano", "English" };
    public static string[] bocadillo2 = { "Biodiversitat", "English" };
    public static string[] rueda = { "Estil de vida\nSostenible", "Castellano", "English" };
}
