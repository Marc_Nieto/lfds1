﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ESRuedaTexto : MonoBehaviour {

    public static string[] bocadillo = { "Vols saber com un\nedifici pot ser \nmés sostenible?", "Castellano", "English" };
    public static string[] bocadillo2 = { "Biodiversitat", "Castellano", "English" };
    public static string[] rueda = { "Edifici\nSostenible", "Castellano", "English" };
    public static string[] bioTitulo = { "Biodiversitat", "Castellano", "English" };
    public static string[] bioDes1 = { "La coberta verda \nde la Fàbrica del Sol", "Castellano", "English" };
    public static string[] bioDes2 = { "La vegetació a La Fàbrica\ndel Sol, naturalitzada", "Castellano", "English" };
    public static string[] bioDes3 = { "La Fàbrica del Sol,\nrefugi per la fauna urbana", "Castellano", "English" };
}
