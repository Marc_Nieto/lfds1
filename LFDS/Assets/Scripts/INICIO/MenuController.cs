﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {
	public GameObject Bocadillo;
	public Animator animator;
    private TextMesh textBocadillo;
    private void Awake()
    {
        textBocadillo = Bocadillo.GetComponent<TextMesh>();
        textBocadillo.text =INICIOTexto.bocadillo[PersistentLanguageController.Idioma];
    }
    public IEnumerator Fabrica(){
		///Bloque para poder controlar entradas y salidas de bocadillos muy util
		Bocadillo = GameObject.Find ("BocadilloMin");
		animator = Bocadillo.GetComponent<Animator>();
		animator.SetTrigger ("Fuera");
		Destroy (Bocadillo, 1);
		///Fin de Bloque
		yield return new WaitForSeconds (1);
		SceneManager.LoadScene ("ESRueda");
	}
	public IEnumerator Bici(){
		Bocadillo = GameObject.Find ("BocadilloMin");
		animator = Bocadillo.GetComponent<Animator>();
		animator.SetTrigger ("Fuera");
		Destroy (Bocadillo, 1);
		yield return new WaitForSeconds (1);
		Debug.Log ("Bici");
		SceneManager.LoadScene ("EVSRueda");
	}
	public IEnumerator Edificios(){
		Bocadillo = GameObject.Find ("BocadilloMin");
		animator = Bocadillo.GetComponent<Animator>();
		animator.SetTrigger ("Fuera");
		Destroy (Bocadillo, 1);
		yield return new WaitForSeconds (1);
		Debug.Log ("Bici");
		SceneManager.LoadScene ("CSRueda");
	}
}
