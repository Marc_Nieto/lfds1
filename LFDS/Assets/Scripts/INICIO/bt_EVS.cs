﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bt_EVS : MonoBehaviour {

	public GameObject Controller;
	private Animator animator;
	private TextMesh textBt;
	void Awake()
	{
		textBt = GetComponent<TextMesh>();
		textBt.text = ESBIOCoberta1Texto.menu_laterial2 [PersistentLanguageController.Idioma];
		animator = GetComponent<Animator> ();
		Controller = GameObject.Find ("Main Camera");
	}
	void OnMouseDown()
	{
		animator.SetTrigger("Clicado");
		Controller.SendMessage("Bici");
	}
}
