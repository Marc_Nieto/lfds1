﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EVSRuedaController : MonoBehaviour {
	private Animator animator;
	private Animator animatorBocadillo;
	public GameObject Bocadillo;
	public GameObject BioTriangulo;
	public GameObject bt_CS;
	public GameObject bt_ES;
	public GameObject bt_EVS;
	public GameObject RuedaController;
	public GameObject BioPlano;
    public GameObject RuedaTexto;

    private TextMesh textBocadillo, textRuedaTexto;
    void Awake()
	{
		animator = GetComponent<Animator>();
        Bocadillo = GameObject.Find("BocadilloMin");
        textBocadillo = Bocadillo.GetComponent<TextMesh>();
        textBocadillo.text = EVSRuedaTexto.bocadillo[PersistentLanguageController.Idioma];
        textRuedaTexto = RuedaTexto.GetComponent<TextMesh>();
        textRuedaTexto.text = EVSRuedaTexto.rueda[PersistentLanguageController.Idioma];
    }
	public IEnumerator Biodiversitat(){
		///Bloque para poder controlar entradas y salidas de bocadillos muy util
		///	Bocadillo = GameObject.Find ("BocadilloMin");
		///	animatorBocadillo = Bocadillo.GetComponent<Animator>();
		///	animatorBocadillo.SetTrigger ("Fuera");
		///	Destroy (Bocadillo, 1);
		///	///Fin de Bloque
		yield return new WaitForSeconds (1);
		///	Instantiate(BioTriangulo, new Vector3(0, -5, -1), Quaternion.identity);
		///	animator.SetTrigger("Fuera");
		///	RuedaController.SendMessage("Fuera");
		///	yield return new WaitForSeconds (1);
		///	Instantiate(bt_CS, new Vector3(0.74f, 2.469f, -5), Quaternion.identity);
		///	Instantiate(bt_EVS, new Vector3(0.74f, 1.835f, -5), Quaternion.identity);
		///	Instantiate(bt_ES, new Vector3(0.74f, 1.203f, -5), Quaternion.identity);
		///	yield return new WaitForSeconds (1);
	}
	public IEnumerator Aire(){
		yield return new WaitForSeconds (1);
		///animator.SetTrigger("Fuera");
		///RuedaController.SendMessage("Fuera");
		yield return new WaitForSeconds (2);
		///SceneManager.LoadScene ("ESAIR");
	}
	public IEnumerator Agua(){
		yield return new WaitForSeconds (1);
		///animator.SetTrigger("Fuera");
		///RuedaController.SendMessage("Fuera");
		yield return new WaitForSeconds (2);
		///SceneManager.LoadScene ("ESAIG");
	}
	public IEnumerator Energia(){
		yield return new WaitForSeconds (1);
		///animator.SetTrigger("Fuera");
		///RuedaController.SendMessage("Fuera");
		yield return new WaitForSeconds (2);
		///SceneManager.LoadScene ("ESENE");
	}
	public IEnumerator Materiales(){
		yield return new WaitForSeconds (1);
		///animator.SetTrigger("Fuera");
		///RuedaController.SendMessage("Fuera");
		yield return new WaitForSeconds (2);
		///SceneManager.LoadScene ("ESMAT");
	}
	public IEnumerator Texto1(){
		animator.SetTrigger ("Sol");
		Instantiate(BioPlano, new Vector3(-2.88f, 1.68f, -2), Quaternion.identity);
		yield return new WaitForSeconds (2);
		SceneManager.LoadScene ("ESBIOCoberta1");
	}
	public IEnumerator Texto2(){
		///animator.SetTrigger ("Sol");
		///Instantiate(BioPlano, new Vector3(-2.88f, 1.68f, -1), Quaternion.identity);
		yield return new WaitForSeconds (2);
		///SceneManager.LoadScene ("ESBIOVegetacio1");
	}
	public IEnumerator Texto3(){
		///animator.SetTrigger ("Sol");
		///Instantiate(BioPlano, new Vector3(-2.88f, 1.68f, -1), Quaternion.identity);
		yield return new WaitForSeconds (2);
		///SceneManager.LoadScene ("ESBIOFauna1");
	}
}
