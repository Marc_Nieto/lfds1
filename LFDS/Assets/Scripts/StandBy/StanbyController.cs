﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;


public class StanbyController : MonoBehaviour {
	public GameObject plano;
	public GameObject Bocadillo;
	public GameObject Sol;
	public GameObject Idiomas;
	private Animator animator;
	private Animator animatorBocadillo;
	private Animator animatorSol;
	private TextMesh textBocadillo;
	void Awake()
	{
		animator = GetComponent<Animator>();
		///Sol = GameObject.Find ("Sol");
		animatorSol = Sol.GetComponent<Animator>();
	}
	void Start(){
		Bocadillo = GameObject.Find ("Bocadillo");
		textBocadillo = Bocadillo.GetComponent<TextMesh>();
		textBocadillo.text=StandByTexto.Saludo[PersistentLanguageController.Idioma];
	}
	public IEnumerator Clicado(){
		Destroy (Idiomas);
		Bocadillo = GameObject.Find ("Bocadillo");
		animatorBocadillo = Bocadillo.GetComponent<Animator>();
		animatorBocadillo.SetTrigger ("Fuera");
		Destroy (Bocadillo, 1);
		yield return new WaitForSeconds(1);
		plano.SendMessage("Desaparece");
		Destroy (plano, 1);
		yield return new WaitForSeconds(1);
		Debug.Log ("Estoy");
		animatorSol.SetTrigger ("Fuera");
		animator.SetTrigger("Fuera");
		yield return new WaitForSeconds(3);
		SceneManager.LoadScene ("INICIO");
	}
		
}
