﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IdiomaClick : MonoBehaviour {
	public GameObject Language;
	public string mensaje;
	void Awake(){
		Language = GameObject.Find ("Idioma");
	}
	void OnMouseDown()
	{
		Language.SendMessage(mensaje);
		SceneManager.LoadScene ("StandBy");
	}
}

