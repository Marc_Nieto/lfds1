﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ESBIOCoberta4Controller : MonoBehaviour {
	///public GameObject Bocadillo;
	public GameObject Seccion;
	public GameObject Titulo;
	public GameObject Contenido;
	public GameObject Resilencia;
	public GameObject Cobertes;
	public GameObject Ciutadania;

	public TextMesh textTitulo, textSeccion, textContenido, textResilencia, textCobertes, textCiutadania, textDesplegable;
    ///Gestion del desplegable
    public GameObject Desplegable;
    public Animator animatorDesplegable;
    private bool desplegado = false;
    /// public TextMesh textBocadillo;
    void Awake(){
		///textBocadillo = Bocadillo.GetComponent<TextMesh>();
		textSeccion = Seccion.GetComponent<TextMesh>();
		textTitulo = Titulo.GetComponent<TextMesh>();
		textContenido = Contenido.GetComponent<TextMesh> ();
		textResilencia = Resilencia.GetComponent<TextMesh> ();
		textCobertes = Cobertes.GetComponent<TextMesh> ();
		textCiutadania = Ciutadania.GetComponent<TextMesh> ();

        animatorDesplegable = Desplegable.GetComponent<Animator>();
        textDesplegable = Desplegable.GetComponent<TextMesh>();

        textCobertes.text=ESBIOCoberta4Texto.mosaic_1[PersistentLanguageController.Idioma];
        textCiutadania.text=ESBIOCoberta4Texto.mosaic_3[PersistentLanguageController.Idioma];
        textResilencia.text=ESBIOCoberta4Texto.mosaic_2[PersistentLanguageController.Idioma];

        textDesplegable.text = ESBIOCoberta4Texto.desplegable[PersistentLanguageController.Idioma];
        textTitulo.text = ESBIOCoberta4Texto.titulo[PersistentLanguageController.Idioma];
		textSeccion.text = ESBIOCoberta4Texto.seccion[PersistentLanguageController.Idioma];
		textContenido.text = ESBIOCoberta4Texto.contenido[PersistentLanguageController.Idioma];

		///textBocadillo.text = ESBIOCoberta4Texto.bocadillo[PersistentLanguageController.Idioma];

	}


	public void Adelante(){
	SceneManager.LoadScene ("ESBIOCoberta5");
	}
	public void Atras(){
	SceneManager.LoadScene ("ESBIOCoberta3");
	}
    ///Bloque para la gestion del desplegable
    public void Pestana()
    {
        if (desplegado == false)
        {
            animatorDesplegable.SetTrigger("Clicado");
            desplegado = true;
        }
        else
        {
            animatorDesplegable.SetTrigger("Desclicado");
            desplegado = false;
        }
    }
    ///Bloque para la gestión de de los botones laterales poner en todas las escenas necesarias
    public IEnumerator Bici(){
	yield return new WaitForSeconds (1);
		SceneManager.LoadScene("EVSRueda");
	}
	public IEnumerator Edificios(){
	yield return new WaitForSeconds (1);
		SceneManager.LoadScene("CSRueda");
	}
	public IEnumerator Fabrica(){
	yield return new WaitForSeconds (1);
		SceneManager.LoadScene("ESRueda");
	}
}
