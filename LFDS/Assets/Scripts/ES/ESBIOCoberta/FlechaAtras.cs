﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlechaAtras : MonoBehaviour {
	public GameObject Controller;
	void OnMouseDown()
	{
		Controller.SendMessage("Atras");
	}
}
