﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonPopUp : MonoBehaviour {

	public GameObject Controller;
	public string Llamada;

	void OnMouseDown()
	{
		Controller.SendMessage(Llamada);
	}
}
