﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ESBIOCoberta3Controller : MonoBehaviour {
	///public GameObject Bocadillo;
	public GameObject Seccion;
	public GameObject Titulo;
	public GameObject Contenido;
	public GameObject Casa1;
	public GameObject Biblioteca1;
	public GameObject CasaTitulo;
	public GameObject CasaContenido;
    public GameObject PopUpCasa;
    public GameObject BibliotecaContenido;
    public GameObject PopUpBiblioteca;
    public GameObject BibliotecaTitulo;
	public TextMesh textTitulo, textSeccion, textContenido, textCasa, textBiblioteca, textCasaTitulo, textCasaContenido, textDesplegable, textBibliotecaContenido, textBibliotecaTitulo;

	public Animator animatorCasa;
    public Animator animatorBiblioteca;
    ///Gestion del desplegable
    public GameObject Desplegable;
    public Animator animatorDesplegable;
    private bool desplegado = false;
    ///public TextMesh textBocadillo;
	void Awake(){
		///textBocadillo = Bocadillo.GetComponent<TextMesh>();
		textSeccion = Seccion.GetComponent<TextMesh>();
		textTitulo = Titulo.GetComponent<TextMesh>();
		textContenido = Contenido.GetComponent<TextMesh> ();
		textCasa = Casa1.GetComponent<TextMesh>();
		textBiblioteca = Biblioteca1.GetComponent<TextMesh>();
        textBibliotecaContenido = BibliotecaContenido.GetComponent<TextMesh>();
        textBibliotecaTitulo = BibliotecaTitulo.GetComponent<TextMesh>();

        animatorDesplegable = Desplegable.GetComponent<Animator>();
        textDesplegable = Desplegable.GetComponent<TextMesh>();

        textCasaContenido =CasaContenido.GetComponent<TextMesh>();
		textCasaTitulo=CasaTitulo.GetComponent<TextMesh>();

        textDesplegable.text = ESBIOCoberta3Texto.desplegable[PersistentLanguageController.Idioma];
        textTitulo.text = ESBIOCoberta3Texto.titulo[PersistentLanguageController.Idioma];
		textSeccion.text = ESBIOCoberta3Texto.seccion[PersistentLanguageController.Idioma];
		textContenido.text = ESBIOCoberta3Texto.contenido[PersistentLanguageController.Idioma];
        textBiblioteca.text=ESBIOCoberta3Texto.rectangulo2[PersistentLanguageController.Idioma];
		textCasa.text = ESBIOCoberta3Texto.rectangulo1[PersistentLanguageController.Idioma];
        textCasaContenido.text = ESBIOCoberta3Texto.descripcion_rectangulo1[PersistentLanguageController.Idioma];
        textBibliotecaContenido.text = ESBIOCoberta3Texto.descripcion_rectangulo2[PersistentLanguageController.Idioma];
        textBibliotecaTitulo.text = ESBIOCoberta3Texto.rectangulo2[PersistentLanguageController.Idioma];

        textCasaTitulo.text = ESBIOCoberta3Texto.rectangulo1[PersistentLanguageController.Idioma];
        animatorBiblioteca = PopUpBiblioteca.GetComponent<Animator>();
        animatorCasa = PopUpCasa.GetComponent<Animator>();

	}


	public void Adelante(){
	SceneManager.LoadScene ("ESBIOCoberta4");
	}
	public void Atras(){
	SceneManager.LoadScene ("ESBIOCoberta2");
	}
	public void Casa(){
    animatorCasa.SetTrigger("Clicado");
    }
    public void CierraCasa(){
       animatorCasa.SetTrigger("Desclicado");
        }
    public void Biblioteca(){
        animatorBiblioteca.SetTrigger("Clicado");
    }
    public void CierraBiblioteca()
    {
        animatorBiblioteca.SetTrigger("Desclicado");
    }
    ///Bloque para la gestion del desplegable
    public void Pestana()
    {
        if (desplegado == false)
        {
            animatorDesplegable.SetTrigger("Clicado");
            desplegado = true;
        }
        else
        {
            animatorDesplegable.SetTrigger("Desclicado");
            desplegado = false;
        }
    }

    ///Bloque para la gestión de de los botones laterales poner en todas las escenas necesarias
    public IEnumerator Bici(){
    	yield return new WaitForSeconds (1);
    		SceneManager.LoadScene("EVSRueda");
    	}
    	public IEnumerator Edificios(){
    	yield return new WaitForSeconds (1);
    		SceneManager.LoadScene("CSRueda");
    	}
    	public IEnumerator Fabrica(){
    	yield return new WaitForSeconds (1);
    		SceneManager.LoadScene("ESRueda");
    	}

}
