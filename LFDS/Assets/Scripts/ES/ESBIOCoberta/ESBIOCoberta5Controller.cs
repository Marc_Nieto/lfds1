﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ESBIOCoberta5Controller : MonoBehaviour {
	public GameObject Bocadillo;
	public GameObject Seccion;
	public GameObject Titulo;
	public GameObject Contenido;

	public TextMesh textTitulo, textSeccion, textContenido, textBocadillo, textDesplegable;
    ///Gestion del desplegable
    public GameObject Desplegable;
    public Animator animatorDesplegable;
    private bool desplegado = false;

    void Awake(){
		textBocadillo = Bocadillo.GetComponent<TextMesh>();
		textSeccion = Seccion.GetComponent<TextMesh>();
		textTitulo = Titulo.GetComponent<TextMesh>();
		textContenido = Contenido.GetComponent<TextMesh> ();

        animatorDesplegable = Desplegable.GetComponent<Animator>();
        textDesplegable = Desplegable.GetComponent<TextMesh>();

        textTitulo.text = ESBIOCoberta5Texto.titulo[PersistentLanguageController.Idioma];
		textSeccion.text = ESBIOCoberta5Texto.seccion[PersistentLanguageController.Idioma];
		textContenido.text = ESBIOCoberta5Texto.contenido[PersistentLanguageController.Idioma];

        textDesplegable.text = ESBIOCoberta5Texto.desplegable[PersistentLanguageController.Idioma];
        textBocadillo.text = ESBIOCoberta5Texto.bocadillo[PersistentLanguageController.Idioma];

	}


	public void Adelante(){
	SceneManager.LoadScene ("ESBIOCoberta6");
	}
	public void Atras(){
	SceneManager.LoadScene ("ESBIOCoberta4");
	}
	///Bloque para la gestión de de los botones laterales poner en todas las escenas necesarias
	public IEnumerator Bici(){
	yield return new WaitForSeconds (1);
		SceneManager.LoadScene("EVSRueda");
	}
	public IEnumerator Edificios(){
	yield return new WaitForSeconds (1);
		SceneManager.LoadScene("CSRueda");
	}
	public IEnumerator Fabrica(){
	yield return new WaitForSeconds (1);
		SceneManager.LoadScene("ESRueda");
	}
    ///Bloque para la gestion del desplegable
	public void Pestana()
    {
        if (desplegado == false)
        {
            animatorDesplegable.SetTrigger("Clicado");
            desplegado = true;
        }
        else
        {
            animatorDesplegable.SetTrigger("Desclicado");
            desplegado = false;
        }
    }
}
