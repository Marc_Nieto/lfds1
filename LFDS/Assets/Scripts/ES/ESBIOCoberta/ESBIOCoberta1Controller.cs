﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ESBIOCoberta1Controller : MonoBehaviour {
	public GameObject Bocadillo;
	public GameObject Oxigen;
	public GameObject Aillant;
	public GameObject Temperatura;
	public GameObject Insectes;
	public GameObject Seccion;
	public GameObject Titulo;
	public GameObject Contenido;
	public GameObject Jardin, Placa, Cielo, Techo;
	public Animator animatorOxigen;
	public Animator animatorAillant;
	public Animator animatorTemperatura;
	public Animator animatorInsectes;
    ///Gestion del desplegable
    public GameObject Desplegable;
    public Animator animatorDesplegable;
    private bool desplegado=false;
	public TextMesh textOxigen, textTitulo, textSeccion, textContenido, textJardin, textTecho, textPlaca, textCielo, textBocadillo, textDesplegable;

	void Awake(){
		animatorOxigen = Oxigen.GetComponent<Animator>();
		animatorAillant = Aillant.GetComponent<Animator>();
		animatorTemperatura = Temperatura.GetComponent<Animator>();
		animatorInsectes = Insectes.GetComponent<Animator>();
        animatorDesplegable = Desplegable.GetComponent<Animator>();
        textDesplegable = Desplegable.GetComponent<TextMesh>();
		textOxigen = Oxigen.GetComponent<TextMesh>();
		textBocadillo = Bocadillo.GetComponent<TextMesh>();
		textSeccion = Seccion.GetComponent<TextMesh>();
		textTitulo = Titulo.GetComponent<TextMesh>();
		textTecho = Techo.GetComponent<TextMesh>();
		textPlaca = Placa.GetComponent<TextMesh>();
		textCielo = Cielo.GetComponent<TextMesh>();
		textJardin= Jardin.GetComponent<TextMesh>();
		textContenido = Contenido.GetComponent<TextMesh>();

		textTitulo.text = ESBIOCoberta1Texto.titulo[PersistentLanguageController.Idioma];
		textSeccion.text = ESBIOCoberta1Texto.seccion[PersistentLanguageController.Idioma];
		textContenido.text = ESBIOCoberta1Texto.contenido[PersistentLanguageController.Idioma];
        textDesplegable.text = ESBIOCoberta1Texto.desplegable[PersistentLanguageController.Idioma];
        textBocadillo.text = ESBIOCoberta1Texto.bocadillo1[PersistentLanguageController.Idioma];

		textJardin.text = ESBIOCoberta1Texto.texto_jardin[PersistentLanguageController.Idioma];
		textCielo.text = ESBIOCoberta1Texto.texto_cielo[PersistentLanguageController.Idioma];
		textPlaca.text = ESBIOCoberta1Texto.texto_placa[PersistentLanguageController.Idioma];
		textTecho.text = ESBIOCoberta1Texto.texto_techo[PersistentLanguageController.Idioma];

	}


	public void OxigenOpen(){
		animatorOxigen.SetBool ("Clicado", true);
		animatorAillant.SetBool ("Clicado", false);
		animatorInsectes.SetBool ("Clicado", false);
		animatorTemperatura.SetBool ("Clicado", false);
	}
	public void OxigenClose(){
		animatorOxigen.SetBool ("Clicado", false);
	}

	public void AillantOpen(){
		animatorAillant.SetBool ("Clicado", true);
		animatorOxigen.SetBool ("Clicado", false);
		animatorInsectes.SetBool ("Clicado", false);
		animatorTemperatura.SetBool ("Clicado", false);
	}
	public void AillantClose(){
		animatorAillant.SetBool ("Clicado", false);
	}

	public void TemperaturaOpen(){
		animatorTemperatura.SetBool ("Clicado", true);
		animatorInsectes.SetBool ("Clicado", false);
		animatorOxigen.SetBool ("Clicado", false);
		animatorAillant.SetBool ("Clicado", false);
	}
	public void TemperaturaClose(){
		animatorTemperatura.SetBool ("Clicado", false);
	}

	public void InsectesOpen(){
		animatorInsectes.SetBool ("Clicado", true);
		animatorOxigen.SetBool ("Clicado", false);
		animatorTemperatura.SetBool ("Clicado", false);
		animatorAillant.SetBool ("Clicado", false);
	}
	public void InsectesClose(){
		animatorInsectes.SetBool ("Clicado", false);
	}

	public void Adelante(){
	SceneManager.LoadScene ("ESBIOCoberta2");
	}
	///public void Atras(){
		///SceneManager.LoadScene ("ESBIOCoberta2");
	///}

	///Bloque para la gestion del desplegable
	public void Pestana(){
        if (desplegado==false){
            animatorDesplegable.SetTrigger("Clicado");
            desplegado = true;
        }
        else{
            animatorDesplegable.SetTrigger("Desclicado");
            desplegado = false;
        }
	}
	///Bloque para la gestión de de los botones laterales poner en todas las escenas necesarias
	public void Bici(){
		SceneManager.LoadScene("EVSRueda");
	}
	public void Edificios(){
		SceneManager.LoadScene("CSRueda");
	}
	public void Fabrica(){
		SceneManager.LoadScene("ESRueda");
	}
}
