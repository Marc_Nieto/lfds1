﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ESBIOCoberta6Controller : MonoBehaviour
{
    public GameObject Bocadillo;
    public GameObject Seccion;
    public GameObject Titulo;
    public GameObject ContenidoFlor;
    public GameObject ContenidoArbol;
    public GameObject ContenidoJardin;
    public GameObject ContenidoCactus;
    public GameObject OcultaMapa;

    public TextMesh textTitulo, textSeccion, textContenidoArbol, textBocadillo, textDesplegable, textContenidoFlor, textContenidoJardin, textContenidoCactus;

    public Animator animatorOcultaMapa;

    ///Gestion del desplegable
    public GameObject Desplegable;
    public Animator animatorDesplegable;
    private bool desplegado = false;
    void Awake()
    {
        textBocadillo = Bocadillo.GetComponent<TextMesh>();
        textSeccion = Seccion.GetComponent<TextMesh>();
        textTitulo = Titulo.GetComponent<TextMesh>();
        textContenidoArbol = ContenidoArbol.GetComponent<TextMesh>();
        textContenidoJardin = ContenidoJardin.GetComponent<TextMesh>();
        textContenidoCactus = ContenidoCactus.GetComponent<TextMesh>();
        textContenidoFlor = ContenidoFlor.GetComponent<TextMesh>();


        animatorDesplegable = Desplegable.GetComponent<Animator>();
        textDesplegable = Desplegable.GetComponent<TextMesh>();

        textTitulo.text = ESBIOCoberta6Texto.titulo[PersistentLanguageController.Idioma];
        textSeccion.text = ESBIOCoberta6Texto.seccion[PersistentLanguageController.Idioma];
        textContenidoArbol.text = ESBIOCoberta6Texto.contenidoArbol[PersistentLanguageController.Idioma];
        textContenidoFlor.text = ESBIOCoberta6Texto.contenidoFlor[PersistentLanguageController.Idioma];
        textContenidoJardin.text = ESBIOCoberta6Texto.contenidoJardin[PersistentLanguageController.Idioma];
        textContenidoCactus.text = ESBIOCoberta6Texto.contenidoCactus[PersistentLanguageController.Idioma];
        textDesplegable.text = ESBIOCoberta6Texto.desplegable[PersistentLanguageController.Idioma];

        textBocadillo.text = ESBIOCoberta6Texto.bocadillo[PersistentLanguageController.Idioma];
        animatorOcultaMapa = OcultaMapa.GetComponent<Animator>();

    }


    public void Adelante()
    {
        SceneManager.LoadScene("ESBIOCoberta7");
    }
    public void Atras()
    {
        SceneManager.LoadScene("ESBIOCoberta5");
    }
    public void QuitaMapa()
    {
        animatorOcultaMapa.SetTrigger("Fuera");
        Destroy(OcultaMapa, 1);
    }
    ///Bloque para la gestion del desplegable
	public void Pestana()
    {
        if (desplegado == false)
        {
            animatorDesplegable.SetTrigger("Clicado");
            desplegado = true;
        }
        else
        {
            animatorDesplegable.SetTrigger("Desclicado");
            desplegado = false;
        }
    }
    ///Bloque para la gestión de de los botones laterales poner en todas las escenas necesarias
    public IEnumerator Bici()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("EVSRueda");
    }
    public IEnumerator Edificios()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("CSRueda");
    }
    public IEnumerator Fabrica()
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene("ESRueda");
    }
}
