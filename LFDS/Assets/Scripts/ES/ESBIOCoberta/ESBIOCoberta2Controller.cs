﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ESBIOCoberta2Controller : MonoBehaviour {
	public GameObject Bocadillo;
	public GameObject Seccion;
	public GameObject Titulo;
	public GameObject Contenido;
	public GameObject OcultaMapa;

	public TextMesh textTitulo, textSeccion, textContenido, textBocadillo, textDesplegable;

    public Animator animatorOcultaMapa;

    ///Gestion del desplegable
    public GameObject Desplegable;
    public Animator animatorDesplegable;
    private bool desplegado = false;
    void Awake(){
		textBocadillo = Bocadillo.GetComponent<TextMesh>();
		textSeccion = Seccion.GetComponent<TextMesh>();
		textTitulo = Titulo.GetComponent<TextMesh>();
		textContenido = Contenido.GetComponent<TextMesh> ();

        animatorDesplegable = Desplegable.GetComponent<Animator>();
        textDesplegable = Desplegable.GetComponent<TextMesh>();

        textTitulo.text = ESBIOCoberta2Texto.titulo[PersistentLanguageController.Idioma];
		textSeccion.text = ESBIOCoberta2Texto.seccion[PersistentLanguageController.Idioma];
		textContenido.text = ESBIOCoberta2Texto.contenido[PersistentLanguageController.Idioma];
        textDesplegable.text = ESBIOCoberta2Texto.desplegable[PersistentLanguageController.Idioma];

        textBocadillo.text = ESBIOCoberta2Texto.bocadillo[PersistentLanguageController.Idioma];
		animatorOcultaMapa= OcultaMapa.GetComponent<Animator>();

	}
		

	public void Adelante(){
	SceneManager.LoadScene ("ESBIOCoberta3");
	}
	public void Atras(){
	SceneManager.LoadScene ("ESBIOCoberta1");
	}
	public void QuitaMapa(){
	animatorOcultaMapa.SetTrigger("Fuera");
	Destroy(OcultaMapa, 1);
	}
    ///Bloque para la gestion del desplegable
	public void Pestana()
    {
        if (desplegado == false)
        {
            animatorDesplegable.SetTrigger("Clicado");
            desplegado = true;
        }
        else
        {
            animatorDesplegable.SetTrigger("Desclicado");
            desplegado = false;
        }
    }
    ///Bloque para la gestión de de los botones laterales poner en todas las escenas necesarias
    public IEnumerator Bici(){
	yield return new WaitForSeconds (1);
		SceneManager.LoadScene("EVSRueda");
	}
	public IEnumerator Edificios(){
	yield return new WaitForSeconds (1);
		SceneManager.LoadScene("CSRueda");
	}
	public IEnumerator Fabrica(){
	yield return new WaitForSeconds (1);
		SceneManager.LoadScene("ESRueda");
	}
}
