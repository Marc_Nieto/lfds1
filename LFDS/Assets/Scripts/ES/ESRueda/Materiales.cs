﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Materiales : MonoBehaviour {

	public GameObject ESRuedaController;
	private Animator animator;
	void Awake()
	{
		animator = GetComponent<Animator>();
	}
	void OnMouseDown()
	{
		animator.SetTrigger("Clicado");
		ESRuedaController.SendMessage("Materiales");
	}
}
