﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ESRuedaController : MonoBehaviour {
	private Animator animator;
	private Animator animatorBocadillo;
	private Animator animatorBioTriangulo;
	public GameObject Bocadillo;
	public GameObject BioTriangulo;
	public GameObject bt_CS;
	public GameObject bt_ES;
	public GameObject bt_EVS;
	public GameObject RuedaController;
	public GameObject BioPlano;
    public GameObject RuedaTexto;

    private TextMesh textBocadillo,textRuedaTexto;
	void Awake()
	{
		animator = GetComponent<Animator>();
        Bocadillo = GameObject.Find("BocadilloMin");
        textBocadillo = Bocadillo.GetComponent<TextMesh>();
        textBocadillo.text = ESRuedaTexto.bocadillo[PersistentLanguageController.Idioma];
        textRuedaTexto = RuedaTexto.GetComponent<TextMesh>();
        textRuedaTexto.text = ESRuedaTexto.rueda[PersistentLanguageController.Idioma];

    }
	public IEnumerator Biodiversitat(){
		///Bloque para poder controlar entradas y salidas de bocadillos muy util
	 	Bocadillo = GameObject.Find ("BocadilloMin");
		animatorBocadillo = Bocadillo.GetComponent<Animator>();
        textBocadillo.text = ESRuedaTexto.bocadillo2[PersistentLanguageController.Idioma];
        yield return new WaitForSeconds(1);
		animatorBocadillo.SetTrigger ("Fuera");
		Destroy (Bocadillo, 1);
		///Fin de Bloque
		yield return new WaitForSeconds (1);
		Instantiate(BioTriangulo, new Vector3(0, -5, -1), Quaternion.identity);
		animator.SetTrigger("Fuera");
		RuedaController.SendMessage("Fuera");
		yield return new WaitForSeconds (1);
		Instantiate(bt_CS, new Vector3(0.74f, 2.469f, -2.8f), Quaternion.identity);
		Instantiate(bt_EVS, new Vector3(0.74f, 1.835f, -2.8f), Quaternion.identity);
		Instantiate(bt_ES, new Vector3(0.74f, 1.203f, -2.8f), Quaternion.identity);
		yield return new WaitForSeconds (1);
	}
	public IEnumerator Aire(){
		yield return new WaitForSeconds (1);
		///animator.SetTrigger("Fuera");
		///RuedaController.SendMessage("Fuera");
		yield return new WaitForSeconds (2);
		///SceneManager.LoadScene ("ESAIR");
	}
	public IEnumerator Agua(){
		yield return new WaitForSeconds (1);
		///animator.SetTrigger("Fuera");
		///RuedaController.SendMessage("Fuera");
		yield return new WaitForSeconds (2);
		///SceneManager.LoadScene ("ESAIG");
	}
	public IEnumerator Energia(){
		yield return new WaitForSeconds (1);
		///animator.SetTrigger("Fuera");
		///RuedaController.SendMessage("Fuera");
		yield return new WaitForSeconds (2);
		///SceneManager.LoadScene ("ESENE");
	}
	public IEnumerator Materiales(){
		yield return new WaitForSeconds (1);
		///animator.SetTrigger("Fuera");
		///RuedaController.SendMessage("Fuera");
		yield return new WaitForSeconds (2);
		///SceneManager.LoadScene ("ESMAT");
	}
	public IEnumerator Texto1(){
		animator.SetTrigger ("Sol");
		Instantiate(BioPlano, new Vector3(-2.88f, 1.68f, -2.5f), Quaternion.identity);
		animatorBioTriangulo = BioTriangulo.GetComponent<Animator>();
        animatorBioTriangulo.SetTrigger("Fuera");
        yield return new WaitForSeconds (2);
		SceneManager.LoadScene ("ESBIOCoberta1");

	}
	public IEnumerator Texto2(){
		///animator.SetTrigger ("Sol");
		///Instantiate(BioPlano, new Vector3(-2.88f, 1.68f, -1), Quaternion.identity);
		yield return new WaitForSeconds (2);
		///SceneManager.LoadScene ("ESBIOVegetacio1");
	}
	public IEnumerator Texto3(){
		///animator.SetTrigger ("Sol");
		///Instantiate(BioPlano, new Vector3(-2.88f, 1.68f, -1), Quaternion.identity);
		yield return new WaitForSeconds (2);
		///SceneManager.LoadScene ("ESBIOFauna1");
	}
	///Bloque para la gestión de de los botones laterales poner en todas las escenas necesarias	
	public void Bici(){
		SceneManager.LoadScene("EVSRueda");
	}
	public void Edificios(){
		SceneManager.LoadScene("CSRueda");
	}
	public void Fabrica(){
		SceneManager.LoadScene("ESRueda");
	}
}
