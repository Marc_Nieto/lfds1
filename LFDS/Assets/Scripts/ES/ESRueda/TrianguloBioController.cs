﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrianguloBioController : MonoBehaviour {

	public GameObject Titulo;
    public GameObject Texto1;
    public GameObject Texto2;
    public GameObject Texto3;
    private TextMesh textTitulo, textTexto1, textTexto2, textTexto3;
    private void Start()
    {
        textTitulo = Titulo.GetComponent<TextMesh>();
        textTexto1 = Texto1.GetComponent<TextMesh>();
        textTexto2 = Texto2.GetComponent<TextMesh>();
        textTexto3 = Texto3.GetComponent<TextMesh>();
        textTitulo.text = ESRuedaTexto.bioTitulo[PersistentLanguageController.Idioma];
        textTexto1.text = ESRuedaTexto.bioDes1[PersistentLanguageController.Idioma];
        textTexto2.text = ESRuedaTexto.bioDes2[PersistentLanguageController.Idioma];
        textTexto3.text = ESRuedaTexto.bioDes3[PersistentLanguageController.Idioma];

    }
}
