﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuedaController : MonoBehaviour {
	private Animator animator;
	void Awake()
	{
		animator = GetComponent<Animator>();
	}
	public void Fuera(){
		animator.SetTrigger("Fuera");
	}
}
