﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentLanguageController : MonoBehaviour {
	public static PersistentLanguageController Instance { get; set;}
	public static int Idioma=0;

	private void Awake (){
		if (Instance == null) {
			Instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}
	public void Catalan(){
		Idioma = 0;
		}
	public void Castellano(){
		Idioma = 1;
	}
	public void Ingles(){
		Idioma = 2;
	}

}
